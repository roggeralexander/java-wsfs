package com.essentiapharma.wsfs.models.beans;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.essentiapharma.wsfs.exceptions.*;
import com.essentiapharma.wsfs.models.beans.UploadedInfo;

public interface FileStorage {
	Resource loadFileFromBucketAsResource(String bucket, String filename) throws StorageFileNotFoundException;
	
	UploadedInfo storeFileInBucket(MultipartFile file, String bucket) throws StorageException;
	
	void removeFileFromBucket(String bucket, String filename) throws StorageFileNotFoundException;

	void removeBucket(String Bucket) throws StorageException;
}
