package com.essentiapharma.wsfs.models.beans;

public class StorageBodyRequest {
	private String service; 
	private String bucket; 
	private String filename; 
	
	public String getService() {
		return this.service;
	}
	
	public StorageBodyRequest setService(String service) {
		this.service = service; 
		return this;
	}

	public String getBucket() {
		return this.bucket;
	}

	public StorageBodyRequest setBucket(String bucket) {
		this.bucket = bucket; 
		return this;
	}
		
	public String getFilename() {
		return this.filename;
	}

	public StorageBodyRequest setFilename(String filename) {
		this.filename = filename; 
		return this;
	}
}