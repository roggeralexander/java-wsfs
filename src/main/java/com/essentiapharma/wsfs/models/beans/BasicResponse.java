package com.essentiapharma.wsfs.models.beans;

public class BasicResponse {
	private String message;
	
	public String getMessage() {
		return this.message;
	}
	
	public BasicResponse setMessage(String message) {
		if(message == null || message.trim().length() == 0) {
			this.message = "";
		}
		
		this.message = message; 
		return this;
	}
}
