package com.essentiapharma.wsfs.models.beans;

public class UploadedInfo {
	private String originalName;
	private String extension;
	private String mimeType;
	private String url;
	private Long size;
	private String humanizedSize;
	private String name;
	private String serviceName;
	private String bucket;
	
	public String getOriginalName() {
		return this.originalName;
	}
	
	public UploadedInfo setOriginalName(String name) {
		this.originalName = name;
		return this;
	}
	
	public String getExtension() {
		return this.extension;
	}
	
	public UploadedInfo setExtension(String extension) {
		this.extension = extension;
		return this;
	}
	
	public String getMimeType() {
		return this.mimeType;
	}
	
	public UploadedInfo setMimeType(String mimeType) {
		this.mimeType = mimeType;
		return this;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	public UploadedInfo setUrl(String url) {
		this.url = url; 
		return this;
	}
	
	public String getHumanizedSize() {
		return this.humanizedSize;
	}
	
	public UploadedInfo setHumanizedSize(String humanizedSize) {
		this.humanizedSize = humanizedSize;
		return this;
	}
	
	public Long getSize() {
		return this.size;
	}
	
	public UploadedInfo setSize(Long size) {
		this.size = size;
		return this;
	}
	
	public String getName() {
		return this.name;
	}
	
	public UploadedInfo setName(String name) {
		this.name = name; 
		return this;
	}
	
	public String getServiceName() {
		return this.serviceName;
	}
	
	public UploadedInfo setServiceName(String serviceName) {
		this.serviceName = serviceName; 
		return this;
	}
	
	public String getBucket() {
		return this.bucket;
	}
	
	public UploadedInfo setBucket(String bucket) {
		this.bucket = bucket; 
		return this;
	}
}
