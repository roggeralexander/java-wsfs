package com.essentiapharma.wsfs.configs;

import org.springframework.boot.context.properties.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;

@ConfigurationProperties("storage")
@Validated
public class StorageProperties {
	@NotNull
	@NotBlank
	@NotEmpty
	private String rootLocation = "uploads"; 
	
	public String getRootLocation() {
		return this.rootLocation;
	}
	
	public StorageProperties setRootLocation(String rootLocation) {
		if(rootLocation != null && rootLocation.trim().length() != 0) {
			this.rootLocation = rootLocation;
		}
		
		return this;
	}
}
