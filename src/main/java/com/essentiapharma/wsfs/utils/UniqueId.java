package com.essentiapharma.wsfs.utils;

import com.fasterxml.uuid.Generators;

public class UniqueId {
	public static String create() {
		String uuid = Generators.randomBasedGenerator().generate().toString();
		return uuid.replaceAll("-", "");
	}
}
