package com.essentiapharma.wsfs.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.essentiapharma.wsfs.exceptions.*;

public class ResolvedLocalStoragePath {
	public static Path getPath(String rootLocation, String bucket) throws StorageException {
		try {
			Path pathResolved = Paths.get(rootLocation);
			pathResolved = pathResolved.resolve(bucket);
			Files.createDirectories(pathResolved);
			return pathResolved;			
		} catch(IOException e) {
			throw new StorageException("Failed to create directory for bucket " + bucket);
		}
	}
	
	public static Path getPath(String rootLocation, String bucket, String filename) throws StorageException {
		Path pathResolved = ResolvedLocalStoragePath.getPath(rootLocation, bucket);
		return pathResolved.resolve(filename);
	}	
}
