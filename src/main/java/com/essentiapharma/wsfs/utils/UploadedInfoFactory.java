package com.essentiapharma.wsfs.utils;

import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.essentiapharma.wsfs.models.beans.UploadedInfo;
import com.essentiapharma.wsfs.utils.UniqueId;

public class UploadedInfoFactory {
	protected static String getExtension(String originalName) {
		return FilenameUtils.getExtension(originalName);
	}
	
	public static UploadedInfo getInstance(MultipartFile file, String serviceName, String bucket) {
		String originalName = file.getOriginalFilename();
		String extension = UploadedInfoFactory.getExtension(originalName);
		Long size = file.getSize();
		String humanizedSize = FileUtils.byteCountToDisplaySize(size);
		
		return (new UploadedInfo()).setOriginalName(originalName)
				.setMimeType(file.getContentType())
				.setSize(size)
				.setExtension(extension)
				.setHumanizedSize(humanizedSize)
				.setServiceName(serviceName)
				.setBucket(bucket)
				.setName(UniqueId.create() + "." + extension);
	}
}
