package com.essentiapharma.wsfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.essentiapharma.wsfs.configs.StorageProperties;
import com.essentiapharma.wsfs.models.beans.BasicResponse;

@RestController
@EnableConfigurationProperties(StorageProperties.class)
@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		final SpringApplication app = new SpringApplication(Application.class);
		app.addListeners(new ApplicationPidFileWriter());
		app.setAddCommandLineProperties(false);
		app.run(args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
			}
		};
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET, produces="application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public BasicResponse index() {
		BasicResponse basicResponse = new BasicResponse();
		basicResponse.setMessage("Bem-vindo ao Web Service ws-fs");
		return basicResponse;
	}
}
