package com.essentiapharma.wsfs.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;

import com.essentiapharma.wsfs.exceptions.*;
import com.essentiapharma.wsfs.models.beans.BasicResponse;
import com.essentiapharma.wsfs.models.beans.StorageBodyRequest;
import com.essentiapharma.wsfs.models.beans.UploadedInfo;
import com.essentiapharma.wsfs.services.LocalStorageService;

@RestController
public class StorageController {
	@Autowired 
	private LocalStorageService localStorageService;
	
	@RequestMapping(value= "/api", method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody 
	public BasicResponse index() {
		return (new BasicResponse()).setMessage("Testando o storage controller");
	}
	
	@RequestMapping(value = "/api/download", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ResponseEntity<Resource> downloadFile(@RequestBody StorageBodyRequest bodyRequest) {
		Resource resource = this.localStorageService.loadFileFromBucketAsResource(
				bodyRequest.getBucket(), bodyRequest.getFilename()
		);
				
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, 
						"attachment; filename=\"" + resource.getFilename() + "\""
				).body(resource);
	}
	
	
	@RequestMapping(value="/files/{bucket}/{filename}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void showFile(HttpServletResponse response, @PathVariable("bucket") String bucket, @PathVariable("filename") String filename) {
		this.localStorageService.copyFileToOutputStream(response, bucket, filename);
		return ; 
	}
	
	@RequestMapping(value="/api/upload/{service}/{bucket}", method = RequestMethod.POST, produces = "application/json") 
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public UploadedInfo uploadFile(@PathVariable("service") String service, @PathVariable("bucket") String bucket, @RequestBody MultipartFile file) {
		return this.localStorageService.storeFileInBucket(file, bucket);
	}
	
	@RequestMapping(value="/api/remove-file", method = RequestMethod.DELETE) 
	@ResponseStatus(HttpStatus.RESET_CONTENT)
	@ResponseBody
	public void removeFile(@RequestBody StorageBodyRequest bodyRequest) {
		this.localStorageService.removeFileFromBucket(bodyRequest.getBucket(), bodyRequest.getFilename());
		return ;
	}

	@RequestMapping(value="/api/remove-bucket", method = RequestMethod.DELETE) 
	@ResponseStatus(HttpStatus.RESET_CONTENT) 
	@ResponseBody
	public void removeBucket(@RequestBody StorageBodyRequest bodyRequest) {
		this.localStorageService.removeBucket(bodyRequest.getBucket());
		return ;
	}
	
	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFoundError(StorageFileNotFoundException e) {
		return ResponseEntity.notFound().build();
	};
	
	@ExceptionHandler(StorageException.class)
	public ResponseEntity<?> handleStorageError(StorageException e) {
		return ResponseEntity.noContent().build();
	};
}
