package com.essentiapharma.wsfs.services;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.net.FileNameMap;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import com.essentiapharma.wsfs.models.beans.*;
import com.essentiapharma.wsfs.configs.StorageProperties;
import com.essentiapharma.wsfs.exceptions.*;
import com.essentiapharma.wsfs.utils.*;

@Service
public class LocalStorageService implements FileStorage {
	private final String serviceName = "ls";
	
	private static final Logger logger = LoggerFactory.getLogger(LocalStorageService.class);

	@Autowired
	private StorageProperties storageProperties;
		
	public Resource loadFileFromBucketAsResource(String bucket, String filename) throws StorageFileNotFoundException {
		String msgError = "Could not read file " + filename + " on bucket " + bucket;
		
		try {
			Path filePath = ResolvedLocalStoragePath.getPath(this.storageProperties.getRootLocation(), bucket, filename);
			Resource resource = new UrlResource(filePath.toUri());
			
			if(!resource.exists() && !resource.isReadable()) {
				throw new StorageFileNotFoundException(msgError);
			}
			
			return resource;
		} catch(MalformedURLException e) {
			throw new StorageFileNotFoundException(msgError, e);
		}		
	}
			
	public UploadedInfo storeFileInBucket(MultipartFile file, String bucket) throws StorageException {
		UploadedInfo uploadedInfo = UploadedInfoFactory.getInstance(file, this.serviceName, bucket);			
		Path filePath = ResolvedLocalStoragePath.getPath(this.storageProperties.getRootLocation(), bucket, uploadedInfo.getName());
		
		try {
			InputStream inputStream = file.getInputStream();	
			Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
			return uploadedInfo.setUrl("http://0.0.0.0:9090/" + filePath.toString().replace(this.storageProperties.getRootLocation(), "files"));
		} catch(IOException e) {
			logger.error("Error salvando o arquivo:" + e.getMessage());
			throw new StorageException("Failed to store file");
		}
	}
	
	public void removeFileFromBucket(String bucket, String filename) throws StorageFileNotFoundException {
		Resource resource = this.loadFileFromBucketAsResource(bucket, filename);

		try {
			resource.getFile().delete();
		} catch(IOException e) {
			throw new StorageFileNotFoundException("Not allowed to remove the file", e);
		}
	}
	
	public void removeBucket(String bucket) throws StorageException {
		Path bucketPath = ResolvedLocalStoragePath.getPath(this.storageProperties.getRootLocation(), bucket);
		
		try { 
			FileSystemUtils.deleteRecursively(bucketPath);
		} catch(IOException e) {
			throw new StorageException("Failed to remove bucket");
		}
	}
	
	public HttpServletResponse copyFileToOutputStream(HttpServletResponse response, String bucket, String filename) throws StorageFileNotFoundException {
		Resource resource = this.loadFileFromBucketAsResource(bucket, filename);

		try {
			FileNameMap fileNameMap = URLConnection.getFileNameMap();
			String mimeType = fileNameMap.getContentTypeFor(resource.getFilename());
			Long size = resource.getFile().length();
			response.setContentType(mimeType);
			response.setContentLength(size.intValue());
			InputStream is = new FileInputStream(resource.getFile());
			IOUtils.copy(is, response.getOutputStream());			
			return response;
		} catch(IOException e) {
			throw new StorageFileNotFoundException("Not allowed to show the file", e);
		}
	}
}